#!/bin/bash

#####################################
#                                   #
#             affichage             #
#                                   #
#####################################

YELLOW='\033[0;33m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0;31m'

function print_begin(){
	echo -e  "${YELLOW}+-----------------------------------------------+"
	echo   "|                                               |"
	printf "|   debut de l'installation pour %-15s|" "$USER"
	echo -e "\n|                                               |"
	echo  -e "+-----------------------------------------------+${NC}"
}

function print_end(){
	echo -e  "${BLUE}+-----------------------------------------------+"
	echo "|                                               |"
	printf "|    fin de l'installation pour %-16s|" "$USER"
	echo -e "\n|                                               |"
	echo  -e "+-----------------------------------------------+${NC}"
}

#####################################
#                                   #
#         main fonction             #
#                                   #
#####################################

function main(){
    print_begin
    install_node
    install_wikijs
    config_wikijs
    install_webserver
    config_webserver

    sudo systemctl start wikijs.service

    print_end
}

#####################################
#                                   #
#        node js fonction           #
#                                   #
#####################################

function install_node(){
    echo -e "${YELLOW}----------- Installation de node.js -----------${NC}"
    sudo apt-get update
    sudo apt-get install -y ca-certificates curl gnupg
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

    NODE_MAJOR=20
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
    
    sudo apt-get update
    sudo apt-get install nodejs -y
}

function install_wikijs(){
    echo -e "${YELLOW}----------- Installation du paquet wikijs -----------${NC}"
    sudo wget https://github.com/Requarks/wiki/releases/latest/download/wiki-js.tar.gz
    sudo mkdir /var/wiki
    sudo tar xzf wiki-js.tar.gz -C /var/wiki
}

function config_wikijs(){
    echo -e "${YELLOW}----------- Configuration de wikijs -----------${NC}"
    sudo cp /srv/git/wikijs/conf/var/wikijs/config.yml /var/wiki

    sudo useradd wikijs -d /var/wiki -M -r
    sudo chown wikijs:wikijs /var/wiki/ -R

    sudo cp /srv/git/wikijs/conf/etc/systemd/system/wikijs.service /etc/systemd/system/
}

#####################################
#                                   #
#      configuration fonction       #
#                                   #
#####################################

function start_wikijs(){
    echo -e "${YELLOW}----------- Démarrage de wikijs -----------${NC}"
    sudo systemctl daemon-reload
    sudo systemctl enable wikijs.service
    sudo systemctl start wikijs.service
}

function install_webserver(){
    echo -e "${YELLOW}----------- Installation de nginx -----------${NC}"
    sudo apt install -y nginx
    sudo systemctl enable nginx.service
    sudo systemctl start nginx.service
}

function config_webserver(){
    echo -e "${YELLOW}----------- Configuration de nginx -----------${NC}"
    sudo cp /srv/git/wikijs/conf/etc/nginx/site-avaible/* /etc/nginx/site-available

    sudo unlink /etc/nginx/site-enabled/default
    sudo ln -s /etc/nginx/site-available/wikijs /etc/nginx/site-enabled/
    sudo ln -s /etc/nginx/site-available/wikijs-ssl /etc/nginx/site-enabled/

    sudo systemctl restart nginx.service

    sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/wiki.key -out /etc/ssl/certs/wiki.crt -subj "/C=FR/ST=France/L=Bourges/O=team-rocket/OU=IT Department/CN=mimir.valhalla.fr"
    sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
}

main