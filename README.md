# wikijs - projet sécurité cloud INSA

- hostname: `mimir`

# Utilisation

Cloner le repo dans le dossier `/srv/git`

Donner les droit d'execution au script d'installation:
```bash
sudo chmod +x /srv/git/wikijs/bin.installation.sh
```

Puis executez le script d'installation:
```bash
/srv/git/wikijs/bin.installation.sh
```

## Certificat generation

```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/wiki.key -out /etc/ssl/certs/wiki.crt -subj "/C=FR/ST=France/L=Bourges/O=team-rocket/OU=IT Department/CN=mimir.valhalla.fr"
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
```

